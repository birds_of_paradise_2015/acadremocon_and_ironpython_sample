﻿# -*- coding: utf-8 -*-
import clr
#AcadRemoconオブジェクト作成
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))

#エラー処理
def Er():
	clr.AddReference('Microsoft.VisualBasic')
	from Microsoft.VisualBasic import Constants
	#ユーザーによるキャンセル
	if (Acad.ErrNumber == Constants.vbObjectError + 1000):
		#ここにキャンセル時の処理を追加
		pass
	else:
		#エラー内容表示
		Acad.ShowError()

def Main():
	#図面全体をDXFファイルに書き出し（ロック画層で描かれた図形も含む）
	if not Acad.acDxfOut("", "DWG"):
		Er()
		exit()
	#DXFファイルからLINEオブジェクトを抽出（10=始点X座標のグループコード,11=終点X座標のグループコード）
	Cnt = clr.Reference[int]()
	ExtArr = clr.Reference[object]()
	if not Acad.DxfExtract(Cnt, ExtArr, "ENTITIES", "", "LINE", "10|11"):
		Er()
		exit()
	#抽出数が0なら終了
	if Cnt.Value == 0:
		exit()
	#始点と終点のX座標を50mmずらす
	for i in range(1, Cnt.Value + 1):
		ExtArr.Value.SetValue("%.10f" % (float(ExtArr.Value.GetValue(1, i)) + 50.0), 1, i)
		ExtArr.Value.SetValue("%.10f" % (float(ExtArr.Value.GetValue(2, i)) + 50.0), 2, i)
	#配列への変更をDXFファイルに反映
	if not Acad.DxfUpdate(ExtArr.Value):
		Er()
		exit()
	#DXFIN実行
	if not Acad.acDxfIn():
		Er()
		exit()
	#画層の状態を保存
	if not Acad.acSaveLayerState():
		Er()
		exit()
	#画層ロック解除（ロック画層で描かれた図形は削除出来ないので）
	if not Acad.acPostCommand("-LAYER U *^M^M"):
		Er()
		exit()
	#直前の選択セットを削除
	if not Acad.acPostCommand("ERASE P^M^M"):
		Er()
		exit()
	#画層の状態を復元
	if not Acad.acLoadLayerState():
		Er()
		exit()

if __name__ == '__main__':
	Main()
