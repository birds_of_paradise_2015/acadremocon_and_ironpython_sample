﻿# -*- coding: utf-8 -*-
def Main():
	from System import Activator, Type
	Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
	#ShadeをアクティブにしてDXFインポート実行
	if not Acad.VbAppActivate("- Shade"):
		exit()
	if not Acad.VbSendKeys("%(FMD)C:\Shade.dxf%(O)"):
		exit()
	#インポート進度表示ウインドウが閉じるまで待つ
	Acad.WaitWindowClose("ＤＸＦファイルの読み込み中...")
	#再度Shadeをアクティブにして図面に合わせて拡大する
	if not Acad.VbAppActivate("- Shade"):
		exit()
	if not Acad.VbSendKeys("%(SF)"):
		exit()
