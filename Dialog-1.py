# -*- coding: utf-8 -*-
import clr
#AcadRemoconオブジェクト作成
from System import Activator, Type, Math
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
Acad.dlLoad("ダイアログサンプル") #ダイアログ開始
Acad.dlAddLabel("", "開始番号")
Acad.dlAddText("txtStart", "123")
Acad.dlShow() #ダイアログ表示
CtrlName = clr.Reference[str]()
while True:
	Acad.dlWaitEvent(CtrlName) #イベント発生待ち
	if CtrlName.Value == "cmdOK":
		Txt = Acad.dlGetProperty("txtStart", "Text")
		break
	elif CtrlName.Value == "cmdCancel":
		break
	else:
		pass
Acad.dlUnload() #ダイアログ終了
clr.AddReference('System.Windows.Forms')
from System.Windows.Forms import MessageBox
if CtrlName.Value == "cmdOK":
	MessageBox.Show("開始番号は「%s」です。" % (Txt))
