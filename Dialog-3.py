# -*- coding: utf-8 -*-
import clr
from System import Activator, Type, Math
clr.AddReference('Microsoft.VisualBasic')
from Microsoft.VisualBasic import Constants
#AcadRemoconオブジェクト作成
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))

#ダイアログ作成&表示
def DialogCreate():
	Acad.dlLoad("プレビューサンプル") #ダイアログ開始
	Acad.dlAddLabel("", "開始値", 12, -1)
	Acad.dlAddLabel("", "プレビュー", 12, 1)
	Acad.dlAddText("txtStart", "1", 10, -1, True)
	Acad.dlIncCurrentX(8)
	Acad.dlAddList("lstPrev", "", 0, 16, 12, -1)
	Acad.dlSetProperty("lstPrev", "TabStop", False) #Tabによるフォーカス移動を無効に
	Acad.dlAddText("", "", 0) #改行のためのダミーコントロール
	Acad.dlAddLabel("", "終了値", 12, 1)
	Acad.dlAddText("txtEnd", "5", 10, 4, True)
	Acad.dlSetProperty("cmdOK", "Visible", False)
	Acad.dlSetProperty("cmdCancel", "Text", "終了")
	Acad.dlShow() #ダイアログ表示

#ダイアログイベント処理
def DialogEvent(CtrlName, CtrlValue, CtrlListIndex):
	#コントロール名で区別
	if CtrlName == "cmdCancel": #キャンセルボタン
		return Constants.vbCancel
	
	elif CtrlName == "txtStart" or CtrlName == "txtEnd": #テキストボックス
		#開始値と終了値を得る
		ST = int(Acad.dlGetProperty("txtStart", "Text"))
		ED = int(Acad.dlGetProperty("txtEnd", "Text"))
		
		#プレビュー開始
		Cnt = 0
		Acad.dlCallMethod("lstPrev", "Clear")
		for i in range(ST, ED + 1):
			Cnt += 1
			if i == ED or Cnt < 9:
				Acad.dlCallMethod("lstPrev", "AddItem", i)
			elif Cnt == 9:
				Acad.dlCallMethod("lstPrev", "AddItem", "途中省略・・・")
		
		#最後の項目を選択
		Acad.dlSetProperty("lstPrev", "ListIndex", Acad.dlGetProperty("lstPrev", "ListCount") - 1)
	
	#再度イベント待ち
	return Constants.vbRetry

#エラー処理
def Er():
	#ユーザーによるキャンセル
	if (Acad.ErrNumber == Constants.vbObjectError + 1000):
		#ここにキャンセル時の処理を追加
		pass
	else:
		#エラー内容表示
		Acad.ShowError()

def Main():
	#バージョンチェック
	if not Acad.CheckVersion("200"): exit()
	
	#ダイアログ作成&表示
	DialogCreate()
	
	#コントロールの初期値でプレビュー実行
	DialogEvent("txtStart", "", -1)
	
	#イベント監視ループ
	CtrlName = clr.Reference[str]()
	CtrlValue = clr.Reference[str]()
	CtrlListIndex = clr.Reference[int]()
	while True:
		#イベント発生待ち
		Acad.dlWaitEvent(CtrlName, CtrlValue, CtrlListIndex)
		
		#イベント処理
		if DialogEvent(CtrlName.Value, CtrlValue.Value, CtrlListIndex.Value) == Constants.vbCancel: return
	
	#ダイアログアンロード
	Acad.dlUnload()

if __name__ == '__main__':
	Main()
