# -*- coding: utf-8 -*-
import clr
#AcadRemoconオブジェクト作成
from System import Activator, Type, Math
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))

#エラー処理
def Er():
	clr.AddReference('Microsoft.VisualBasic')
	from Microsoft.VisualBasic import Constants
	#ユーザーによるキャンセル
	if (Acad.ErrNumber == Constants.vbObjectError + 1000):
		#ここにキャンセル時の処理を追加
		pass
	else:
		#エラー内容表示
		Acad.ShowError()

def Main():
	#バージョンチェック
	if not Acad.CheckVersion("220"): exit()

	#ブロック抽出
	if not Acad.acDxfOut("ブロックを選択", "", False):
		Er()
		exit()
	BlkCnt = clr.Reference[int]()
	Blk = clr.Reference[object]()
	if not Acad.DxfExtract(BlkCnt, Blk, "BLOCKS", "", "BLOCK", "2"):
		Er()
		exit()
	if BlkCnt.Value == 0: exit()

	#ブロック数ループ
	LineCnt = 0
	ObjCnt = clr.Reference[int]()
	Obj = clr.Reference[object]()
	for i in range(1, BlkCnt.Value + 1):
		#モデル空間とスペース空間の定義は無視
		if str(Blk.Value.GetValue(1, i))[0] != "*":
			#線分を取得
			if not Acad.DxfExtractBlock(ObjCnt, Obj, Blk.Value.GetValue(Blk.Value.GetUpperBound(0), i), "LINE", "10|20"):
				Er()
				exit()
			LineCnt += ObjCnt.Value

	#抽出結果表示
	Acad.acShowMessage("%d本の線分を抽出しました。" % (LineCnt))

if __name__ == '__main__':
	Main()
