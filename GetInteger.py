﻿# -*- coding: utf-8 -*-
from __future__ import print_function
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
retValue = clr.Reference[int]()
Acad.GetInteger("文字の高さを入力", retValue, 12, "=10", "<20")
print(retValue.Value)
