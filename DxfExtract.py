# -*- coding: utf-8 -*-
import clr
#AcadRemoconオブジェクト作成
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))

#エラー処理
def Er():
	clr.AddReference('Microsoft.VisualBasic')
	from Microsoft.VisualBasic import Constants
	#ユーザーによるキャンセル
	if (Acad.ErrNumber == Constants.vbObjectError + 1000):
		#ここにキャンセル時の処理を追加
		pass
	else:
		#エラー内容表示
		Acad.ShowError()

#選択した直線を50mm平行移動
def Main():
	#図形選択→DXFファイル書き出し（ロックされた画層で描かれた図形は選択不可）
	if not Acad.acDxfOut("線分を選択", "", False):
		Er()
		exit()
	Cnt = clr.Reference[int]()
	ExtArr = clr.Reference[object]()
	#DXFファイルからLINEオブジェクトを抽出（10=始点X座標のグループコード,11=終点X座標のグループコード）
	if not Acad.DxfExtract(Cnt, ExtArr, "ENTITIES", "", "LINE", "10|11"):
		Er()
		exit()
	#抽出数が0なら終了
	if Cnt.Value == 0:
		exit()
	#始点と終点のX座標を50mmずらす
	for i in range(1, Cnt.Value + 1):
		ExtArr.Value.SetValue(float(ExtArr.Value.GetValue(1, i)) + 50.0, 1, i)
		ExtArr.Value.SetValue(float(ExtArr.Value.GetValue(2, i)) + 50.0, 2, i)
	#配列への変更をDXFファイルに反映
	if not Acad.DxfUpdate(ExtArr):
		Er()
		exit()
	#DXFIN実行
	if not Acad.acDxfIn():
		Er()
		exit()
	#直前の選択セットを削除
	if not Acad.acPostCommand("ERASE P^M^M"):
		Er()
		exit()

if __name__ == '__main__':
	Main()
