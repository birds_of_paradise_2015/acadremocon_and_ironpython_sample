# -*- coding: utf-8 -*-
from __future__ import print_function
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
clr.AddReference('Microsoft.VisualBasic')
from Microsoft.VisualBasic import Constants
Ret=Acad.acShowMessage("メッセージ1", Constants.vbCritical | Constants.vbOKOnly)
print(Ret)
Ret=Acad.acShowMessage("メッセージ2", Constants.vbExclamation | Constants.vbYesNo | Constants.vbDefaultButton1)
print(Ret)
Ret=Acad.acShowMessage("メッセージ3", Constants.vbExclamation | Constants.vbOKCancel | Constants.vbDefaultButton2)
print(Ret)
Ret=Acad.acShowMessage("メッセージ4", Constants.vbInformation | Constants.vbAbortRetryIgnore | Constants.vbDefaultButton3)
print(Ret)
