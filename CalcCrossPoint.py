﻿# -*- coding: utf-8 -*-
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
X = clr.Reference[float]()
Y = clr.Reference[float]()
"""
PX1 = 0.0
PY1 = 0.0
PX2 = 5.0
PY2 = 5.0
QX1 = 3.0
QY1 = 0.0
QX2 = 4.0
QY2 = 6.0
"""
LngRet = Acad.CalcCrossPoint(X, Y, PX1, PY1, PX2, PY2, QX1, QY1, QX2, QY2)
clr.AddReference('System.Windows.Forms')
from System.Windows.Forms import MessageBox
if LngRet == 0:
	MessageBox.Show("2直線は平行です。")
elif LngRet == 1:
	MessageBox.Show("2直線は(%f, %f)で交わります。" % (X.Value, Y.Value))
elif LngRet == 2:
	MessageBox.Show("2直線は仮想交点(%f, %f)で交わります。" % (X.Value, Y.Value))
else:
	pass
