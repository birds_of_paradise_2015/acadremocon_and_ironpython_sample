﻿# -*- coding: utf-8 -*-
from __future__ import print_function
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
FileName = Acad.shShowOpen("AutoCAD図面 (*.dwg)|*.dwg")
print(FileName)
FileName = Acad.shShowOpen("AutoCAD図面 (*.dwg;*.dxf)|*.dwg;*.dxf")
print(FileName)
FileName = Acad.shShowOpen("AutoCAD図面 (*.dwg)|*.dwg|すべてのファイル (*.*)|*.*")
print(FileName)
