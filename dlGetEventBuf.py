# -*- coding: utf-8 -*-
import clr
#AcadRemoconオブジェクト作成
from System import Activator, Type, Math
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))

#エラー処理
def Er():
	clr.AddReference('Microsoft.VisualBasic')
	from Microsoft.VisualBasic import Constants
	#ユーザーによるキャンセル
	if (Acad.ErrNumber == Constants.vbObjectError + 1000):
		#ここにキャンセル時の処理を追加
		pass
	else:
		#エラー内容表示
		Acad.ShowError()

#ダイアログ作成＆表示
def DialogCreate():
	Acad.dlLoad("イベントバッファ処理", None, True)
	Acad.dlAddLabel("lblInfo", "直線を1000本引きます。", 22, 2)
	Acad.dlIncCurrentX(30)
	Acad.dlAddButton("cmdCancel", "中断", 6, -1)
	Acad.dlShow(0, 0)

def Main():
	#バージョンチェック
	if not Acad.CheckVersion("214"): exit()

	#ダイアログ作成&表示
	DialogCreate()

	#UNDO記録開始
	if not Acad.acPostCommand("UNDO BE "):
		Er()
		exit()

	#描画＆イベント監視ループ
	Acad.dlEventBufStart = True #イベントバッファ開始
	CtrlName = clr.Reference[str]()
	CtrlValue = clr.Reference[object]()
	CtrlListIndex = clr.Reference[int]()
	for i in range(1, 1000 + 1):
		if not Acad.acPostCommand("LINE " + Acad.PT(i,0) + Acad.PT(i,1000) + " "):
			Er()
			exit()
		Acad.dlSetProperty("lblInfo", "TEXT", "%d本目です..." % (i))
		if Acad.dlGetEventBuf(CtrlName, CtrlValue, CtrlListIndex):
			if CtrlName.Value == "cmdCancel":
				Acad.acShowMessage("中断しました。")
				break

	#後処理
	Acad.dlUnload()
	Acad.acPostCommand("UNDO E ")
	Acad.acPostCommand("UNDO 1 ")

if __name__ == '__main__':
	Main()
