﻿# -*- coding: utf-8 -*-
"""
使用例1

ヒストリー文字列全体を取得します
"""
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
retHistory = clr.Reference[str]()
Acad.acGetHistory(retHistory)
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox
MessageBox.Show(retHistory.Value)
