﻿# -*- coding: utf-8 -*-
"""
使用例3

「X =」「Y =」「Z =」で区切られたヒストリーを抽出します
"""
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
retHistory = clr.Reference[object]()
Acad.acGetHistory(retHistory, "X =|Y =|Z =")
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox
MessageBox.Show(retHistory.Value[0])
MessageBox.Show(retHistory.Value[1])
MessageBox.Show(retHistory.Value[2])
