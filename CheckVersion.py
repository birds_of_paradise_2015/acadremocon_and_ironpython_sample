﻿# -*- coding: utf-8 -*-
def Main():
	from System import Activator, Type
	Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
	if not Acad.CheckVersion("110"):
		exit()

if __name__ == '__main__':
	Main()
