﻿# -*- coding: utf-8 -*-
"""
使用例2

「LAYER」コマンド以降のヒストリー文字列を取得します
"""
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
retHistory = clr.Reference[str]()
Acad.acGetHistory(retHistory, "LAYER")
clr.AddReference("System.Windows.Forms")
from System.Windows.Forms import MessageBox
MessageBox.Show(retHistory.Value)
