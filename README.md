# acadremocon_and_ironpython_sample

IronPythonにてAcadRemocon.dllを利用するサンプルファイル郡。

-----

AcadRemocon.dllの「[メソッド一覧](http://hp.vector.co.jp/authors/VA026873/AcadRemoconHelp/Commands.htm)」
にある各メソッドの「使用例」をIronPythonに移植しています。

IronRubyによる「[使用例](https://bitbucket.org/birds_of_paradise_2015/acadremocon_and_ironpython_sample)」もあります。

## 実行方法

1. IronPythonをインストールしてください。

1. サンプルコードをダウンロードし、任意のフォルダに解凍します。

1. コマンドプロンプト上で `ipy acGetVar.py` 等とタイプして、実行してください。

## 動作確認環境

- Windows 7 Professional (64 bit)
- AutoCAD 2011 (64 bit)
- AcadRemocon.dll 3.4.0.3
- IronPython 2.7.5

## ライセンス

[the MIT License](http://opensource.org/licenses/mit-license.php)
で公開しています。

## リンク

[IronRuby - Home](http://ironruby.codeplex.com/)

## 履歴

- 2010/09/03 acadremocon.netホーム 掲示板 にて公開
- 2015/12/18 再公開
