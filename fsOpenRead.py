﻿# -*- coding: utf-8 -*-
"""

シーケンシャルファイル処理サンプルプログラム

"""
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
#■ファイルを新規作成モードで開く
f = Acad.fsOpenWrite(r"C:\Tmp.txt") #正常なら1以上のファイル番号を返す
Acad.fsWriteLine(f, "ABC")
Acad.fsClose()

#■ファイルを追記モードで開く
f = Acad.fsOpenAppend(r"C:\Tmp.txt")
Acad.fsWriteLine(f, "EFG")
Acad.fsClose()

#■ファイルを読み取りモードで開く
StrRead = ""
f = Acad.fsOpenRead(r"C:\Tmp.txt")
while not Acad.fsEOF(f): #ファイル終端までループ
	StrRead += Acad.fsReadLine(f) + "\r\n"
Acad.fsClose()

#■ファイル内容表示
import clr
clr.AddReference('System.Windows.Forms')
from System.Windows.Forms import MessageBox
MessageBox.Show(StrRead)
