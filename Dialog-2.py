# -*- coding: utf-8 -*-
import clr
from System import Activator, Type, Math
clr.AddReference('Microsoft.VisualBasic')
from Microsoft.VisualBasic import Constants
#AcadRemoconオブジェクト作成
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))

#DialogCreate(), Main()で使用しているのでこの位置で変数をとりあえず宣言しておく。
Keta = None #dummy

#ダイアログ作成&表示
def DialogCreate():
	Acad.dlLoad("ダイアログサンプル") #ダイアログ開始
	Acad.dlAddCheck("chkKeta", "桁を揃える", Keta, 16, 1)
	Acad.dlIncCurrentX(4)
	Acad.dlAddLabel("", "桁数", 4, -1, True)
	Acad.dlAddText("txtKeta", "0", 11, 4, True)
	Acad.dlAddLabel("", "色", 16, 1)
	#アイテム値を利用して色名に対応した数値を格納
	Acad.dlAddList("lstColo", "黒|赤|緑|黄|青|マゼンタ|シアン|白", 0, 16, 8, 0, "0|255|65280|65535|16711680|16711935|16776960|16777215")
	Acad.dlShow() #ダイアログ表示

#ダイアログイベント処理
def DialogEvent(CtrlName, CtrlValue, CtrlListIndex):
	#コントロール名で区別
	if CtrlName == "cmdOK": #OKボタン
		#桁数に1以上の値が入っていればOK
		if int(Acad.dlGetValue("txtKeta")) >= 1: return Constants.vbOK
		#メッセージ表示
		Acad.dlShowMessage("桁数には1以上の数値を入力して下さい", Constants.vbExclamation | Constants.vbOKOnly)

	elif CtrlName == "cmdCancel": #キャンセルボタン
		return Constants.vbCancel
	
	elif CtrlName == "chkKeta": #桁揃えチェックボックス
		#桁数テキストボックスの無効化
		if CtrlValue == 0:
			Acad.dlSetProperty("txtKeta", "Enabled", False)
			
		#桁数テキストボックスを有効化
		else:
			Acad.dlSetProperty("txtKeta", "Enabled", True)

	#色リストボックス
	elif CtrlName == "lstColo":
		#アイテム値を取得
		BC = Acad.dlGetProperty("lstColo", "ItemData", CtrlListIndex)
		
		#背景色と前景色を設定
		Acad.dlSetProperty("lstColo", "BackColor", BC)
		Acad.dlSetProperty("lstColo", "ForeColor", 0xFFFFFF - BC)
	
	#再度イベント待ち
	return Constants.vbRetry

#エラー処理
def Er():
	#ユーザーによるキャンセル
	if (Acad.ErrNumber == Constants.vbObjectError + 1000):
		#ここにキャンセル時の処理を追加
		pass
	else:
		#エラー内容表示
		Acad.ShowError()

def Main():
	#バージョンチェック
	if not Acad.CheckVersion("200"): exit()
	
	#ダイアログ作成&表示
	DialogCreate()
	
	#設定値取得
	Keta = clr.Reference[str]()
	Colo = clr.Reference[str]()
	Acad.GetIniVal(Keta, "Keta", "DialogSample")
	Acad.GetIniVal(Colo, "Colo", "DialogSample")
	
	#初期値設定(値を設定後、イベントルーチンを呼び出してコントロールを初期化する)
	Acad.dlSetProperty("chkKeta", "Value", Keta)
	Acad.dlSetProperty("lstColo", "ListIndex", Colo)
	DialogEvent("chkKeta", Keta, -1)
	DialogEvent("lstColo", "", Colo)
	
	#イベント監視ループ
	CtrlName = clr.Reference[str]()
	CtrlValue = clr.Reference[str]()
	CtrlListIndex = clr.Reference[int]()
	while True:
		#イベント発生待ち
		Acad.dlWaitEvent(CtrlName, CtrlValue, CtrlListIndex)
		
		#イベント処理
		_event = DialogEvent(CtrlName.Value, CtrlValue.Value, CtrlListIndex.Value)
		if _event == Constants.vbOK:
			break
		elif _event == Constants.vbCancel:
			return
	
	#コントロールの値取得(ダイアログのアンロード前に行う)
	Keta = Acad.dlGetProperty("chkKeta", "Value")
	Colo = Acad.dlGetProperty("lstColo", "ListIndex")
	
	#ダイアログアンロード
	Acad.dlUnload()
	
	#設定値保存
	Acad.PutIni(Keta, "Keta", "DialogSample")
	Acad.PutIni(Colo, "Colo", "DialogSample")

if __name__ == '__main__':
	Main()
