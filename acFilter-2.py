﻿# -*- coding: utf-8 -*-
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
Acad.acFilter("線分の始点", ">", "0", ">", "0", ">", "0", False)
Acad.acFilter("線分の終点", "<", "100", "<", "100", "<", "100", False)
Acad.acFilter("線種", "=", "Continuous")
