# -*- coding: utf-8 -*-
#選択した図形（文字）の高さを10に、角度を45度にするサンプル
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
Cnt = clr.Reference[int]()
Acad.acSelect("文字を選択",Cnt)
if Cnt.Value > 0:
	Acad.acPostCommand("CHANGE P ")
	rIndex = clr.Reference[int]()
	while True:
		Acad.acSkipPrompt("新しい高さ*|新しい角度*", rIndex)
		if rIndex.Value == -1:
			break #「コマンド：」に戻った
		elif rIndex.Value == 0:
			Acad.acPostCommand("10 ")
		elif rIndex.Value == 1:
			Acad.acPostCommand("45 ")
		else:
			pass
