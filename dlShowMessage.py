# -*- coding: utf-8 -*-
from __future__ import print_function
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
clr.AddReference('Microsoft.VisualBasic')
from Microsoft.VisualBasic import Constants
Ret=Acad.dlShowMessage("メッセージ1", Constants.vbCritical | Constants.vbOKOnly)
print(Ret)
Ret=Acad.dlShowMessage("メッセージ2", Constants.vbExclamation | Constants.vbYesNo | Constants.vbDefaultButton1)
print(Ret)
Ret=Acad.dlShowMessage("メッセージ3", Constants.vbExclamation | Constants.vbOKCancel | Constants.vbDefaultButton2)
print(Ret)
Ret=Acad.dlShowMessage("メッセージ4", Constants.vbInformation | Constants.vbAbortRetryIgnore | Constants.vbDefaultButton3)
print(Ret)
