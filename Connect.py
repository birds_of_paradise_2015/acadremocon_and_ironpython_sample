﻿# -*- coding: utf-8 -*-
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
if Acad.Connect():
	Acad.acShowMessage("AutoCADを発見しました。")
else:
	import clr
	clr.AddReference("System.Windows.Forms")
	from System.Windows.Forms import MessageBox
	MessageBox.Show("AutoCADを発見出来ませんでした。")
