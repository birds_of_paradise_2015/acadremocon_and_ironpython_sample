﻿# -*- coding: utf-8 -*-
from System import Activator, Type
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))
import clr
LayerCount = clr.Reference[int]()
Acad.acGetLayerList(LayerCount)
Msg =  "画層名\t表示\tフリーズ\tロック\t"
Msg += "色\t線種\t太さ\t印刷\n\n"
LN = clr.Reference[str]()
Vi = clr.Reference[str]()
Fr = clr.Reference[str]()
Lo = clr.Reference[str]()
Co = clr.Reference[str]()
LT = clr.Reference[str]()
LW = clr.Reference[str]()
PO = clr.Reference[str]()
for i in range(1, LayerCount.Value + 1):
	Acad.GetLayerInfo(i, LN, Vi, Fr, Lo, Co, LT, LW, PO)
	Msg += LN.Value + "\t" + Vi.Value + "\t" + Fr.Value + "\t" + Lo.Value + "\t"
	Msg += Co.Value + "\t" + LT.Value + "\t" + LW.Value + "\t" + PO.Value + "\n"
clr.AddReference('Microsoft.VisualBasic')
from Microsoft.VisualBasic import Constants
Acad.acShowMessage(Msg, Constants.vbInformation, "画層一覧表示サンプル")
