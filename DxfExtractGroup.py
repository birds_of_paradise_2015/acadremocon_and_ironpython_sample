# -*- coding: utf-8 -*-
import clr
#AcadRemoconオブジェクト作成
from System import Activator, Type, Math
Acad = Activator.CreateInstance(Type.GetTypeFromProgID('AcadRemocon.Body'))

#エラー処理
def Er():
	clr.AddReference('Microsoft.VisualBasic')
	from Microsoft.VisualBasic import Constants
	#ユーザーによるキャンセル
	if (Acad.ErrNumber == Constants.vbObjectError + 1000):
		#ここにキャンセル時の処理を追加
		pass
	else:
		#エラー内容表示
		Acad.ShowError()

def Main():
	#バージョンチェック
	if not Acad.CheckVersion("220"): exit()
	#ポリライン抽出
	if not Acad.acDxfOut("ポリラインを選択", "", False):
		Er()
		exit()
	ObjCnt = clr.Reference[int]()
	Obj = clr.Reference[object]()
	if not Acad.DxfExtract(ObjCnt, Obj, "ENTITIES", "", "LWPOLYLINE", "8"):
		Er()
		exit()
	if ObjCnt.Value == 0: exit()
	GrpCnt = clr.Reference[int]()
	Grp = clr.Reference[object]()
	for i in range(ObjCnt.Value, 0, -1):
		if not Acad.DxfExtractGroup(GrpCnt, Grp, Obj.Value.GetValue(Obj.Value.GetUpperBound(0), i), "10|20"):
			Er()
			exit()
		#頂点数が2個以上で始点と終点が異なる場合は始点と同じ座標の頂点データを追加
		if GrpCnt.Value > 2 and \
			Math.Abs(float(Grp.Value.GetValue(1, GrpCnt.Value)) - float(Grp.Value.GetValue(1, 1))) > 1.0 and \
			Math.Abs(float(Grp.Value.GetValue(2, GrpCnt.Value)) - float(Grp.Value.GetValue(2, 1))) > 1.0:
			Grp.Value.SetValue( \
				"%s\n10\n%s\n20\n%s" % (Grp.Value.GetValue(2, GrpCnt.Value), \
				Grp.Value.GetValue(1, 1), \
				Grp.Value.GetValue(2, 1)), \
				2, GrpCnt.Value)
	if not Acad.DxfUpdate(Grp):
		Er()
		exit()
	#直前の選択セットを削除
	if not Acad.acPostCommand("UNDO BE "): #UNDO記録開始
		Er()
		exit()
	if not Acad.acDxfIn(): #DXF読み込み
		Er()
		exit()
	if not Acad.acPostCommand("ERASE P "): #直前の選択セットを削除
		Er()
		exit()
	if not Acad.acPostCommand("UNDO E "): #UNDO記録終了
		Er()
		exit()

if __name__ == '__main__':
	Main()
